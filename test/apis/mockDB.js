import { BaseDataInterface } from "@brownpapertickets/surya-gql-data"

export class MockDB extends BaseDataInterface {
  constructor(container) {
    super(container)
  }

  async connect(connectionString, isProduction) {
    this.log.info(
      `MockDB Connect: ${connectionString} isProduction: ${isProduction}`
    )
    const apis = this.getAllAPIs()
    const typeSpecs = this.getTypeSpecs()

    this.log.info(`MockDB APIs: ${Object.keys(apis)}`)
    // this.log.info(`MockDB typeSpecs: ${JSON.stringify(typeSpecs, null, 2)}`)
    return this
  }

  async disconnect() {
    /** Nothing to do */
  }
}
