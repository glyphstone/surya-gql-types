import { BaseCRUDAPI } from "@brownpapertickets/surya-gql-data"

const DEFAULT_LIMIT = 100

export class MockAPI extends BaseCRUDAPI {
  constructor(container, type, typeMap) {
    super(container, type, typeMap)
    this.log.info(`Intantiate Mock API`)
    this.db = container.db
  }

  async query(
    offset = 0,
    limit = DEFAULT_LIMIT,
    filter = "",
    sort = "",
    session
  ) {
    this.log.info(`Mock Events API Query`)
    return {}
  }

  async get(id, session) {
    this.log.info(`Mock Events API Get`)
    return {}
  }

  async create(obj, session) {
    this.log.info(`Mock Events API Create`)
    return {}
  }

  async update(id, obj, session) {
    this.log.info(`Mock Events API Update`)
    return {}
  }

  async delete(id, session) {
    this.log.info(`Mock Events API Delete`)
    return {}
  }
}
