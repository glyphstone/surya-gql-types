import { BaseType, Permissions } from "../../../src/baseType"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Account extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return {
      AccountClaims,
      AuthToken,
    }
  }

  typeSpec() {
    return {
      name: "Account",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          ownerId: true,
          input: false,
          asName: "_id",
        },
        status: {
          type: ScalarTypes.Enum,
          values: ["ACTIVE", "INACTIVE", "BANNED"],
        },
        claims: {
          type: ScalarTypes.Object,
          objectType: "AccountClaims",
          readPermission: Permissions.Personal,
        },
        emailToken: {
          type: ScalarTypes.Object,
          objectType: "AuthToken",
          readPermission: Permissions.Personal,
        },
        createdDate: {
          type: ScalarTypes.DateTime,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
        },
      },
    }
  }
}

export class AccountClaims extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return {}
  }

  typeSpec() {
    return {
      name: "AccountClaims",
      extends: null,
      instantiable: false,
      fields: {
        accountId: {
          type: ScalarTypes.String,
          index: true,
          unique: true,
          asName: "sub",
        },
        email: {
          type: "EmailScalar",
          index: true,
          unique: true,
          readPermission: Permissions.Personal,
        },
        emailVerified: {
          type: ScalarTypes.Boolean,
          asName: "email_verified",
        },
        name: {
          type: ScalarTypes.String,
        },
        preferredUserName: {
          type: ScalarTypes.String,
          asName: "preferred_username",
        },
      },
    }
  }
}

export class AuthToken extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return {}
  }

  typeSpec() {
    return {
      name: "AuthToken",
      extends: null,
      instantiable: false,
      fields: {
        value: {
          type: ScalarTypes.String,
        },
        created: {
          type: ScalarTypes.DateTime,
        },
      },
    }
  }
}
