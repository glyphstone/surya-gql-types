import { BaseType } from "../../../src/baseType"

export class Simple extends BaseType {
  typeSpec() {
    return {
      name: "Simple",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: "ID",
          key: true,
          required: true,
          input: false,
        },
        simpleValue: {
          type: "String",
        },
        email: {
          type: "EmailScalar",
          baseType: "String",
        },
      },
    }
  }
}

export class SimpleName extends BaseType {
  typeSpec() {
    return {
      name: "SimpleName",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        firstName: {
          type: "String",
        },
        lastName: {
          type: "String",
        },
      },
    }
  }
}
