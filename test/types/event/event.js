import { BaseType } from "../../../src/baseType"

export class Event extends BaseType {
  typeSpec() {
    return {
      name: "Event",
      extends: null,
      instantiable: true,
      inputs: ["EventVenueInput"],
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: "ID",
          key: true,
          required: true,
          input: false,
        },
        passesTest: {
          type: "Boolean",
        },
        capacity: {
          type: "Integer",
        },
        gender: {
          type: "Enum",
          values: ["TRANS", "NON-BINARY", "FEMALE", "MALE"],
        },
        color: {
          type: "Enum",
          name: "RGB",
        },
      },
    }
  }
}
