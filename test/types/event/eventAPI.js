import { MockAPI } from "../../apis/mockAPI"

export class EventAPI extends MockAPI {
  constructor(container, type, typeMap) {
    super(container, type, typeMap)
  }

  getCollection() {
    return this.db.collections["Event"]
  }
}
