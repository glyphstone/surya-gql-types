export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.1",
  description: "New MongoDB Based Events",
  types: ["Event"],
  queries: "EventQueries",
  resolvers: "EventResolvers",
  manager: "EventManager",
  data: { interface: "MockDB", api: "EventAPI" },
}
