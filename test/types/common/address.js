import { BaseType } from "../../../src/baseType"

export class Address extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Address",
      extends: null,
      instantiable: false,
      fields: {
        address1: {
          type: "String",
        },
        address2: {
          type: "String",
        },
        city: {
          type: "String",
        },
        state: {
          type: "String",
        },
        postalCode: {
          type: "String",
        },
        country: {
          type: "String",
        },
      },
    }
  }
}
