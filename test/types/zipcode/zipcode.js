import { BaseType } from "../../../src/baseType"

export class Zipcode extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return { Coordinates }
  }

  typeSpec() {
    return {
      name: "Zipcode",
      extends: null,
      instantiable: true,
      timestamps: false,
      permissions: {
        secure: ["ADMINISTRATOR"],
      },
      fields: {
        id: {
          type: "Integer",
          asName: "zipcode_id",
          key: true,
          required: true,
          input: false,
        },
        zipcode: {
          type: "String",
        },
        city: {
          type: "String",
        },
        state: {
          type: "String",
        },
        county: {
          type: "String",
        },
        country: {
          type: "String",
        },
        areaCode: {
          type: "String",
          asName: "areacode",
        },
        cityType: {
          type: "String",
          asName: "cityType",
        },
        cityAliasAbbreviation: {
          type: "String",
          asName: "cityaliasabbreviation",
        },
        cityAliasName: {
          type: "String",
          asName: "cityaliasname",
        },
        latitude: {
          type: "Float",
        },
        longitude: {
          type: "Float",
        },
        timezone: {
          type: "Integer",
        },
        distanceMiles: {
          type: "Float",
          symbolic: true,
          params: "(lat: Float, lon: Float)",
        },
      },
    }
  }
}
