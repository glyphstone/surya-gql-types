import { BaseCRUDManager } from "@brownpapertickets/surya-gql-types"

const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["NOBODY"] },
  update: { groups: ["ADMINISTRATOR"] },
  delete: { groups: ["NOBODY"] },
}

export class ZipcodeManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }
}
