import { gql } from "apollo-server"

export const ZipcodeQueries = gql`
  extend type Query {
    zipcodes (
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): ZipcodeSetResponse
  }
`
