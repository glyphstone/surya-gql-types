import { gql } from "apollo-server"

export const UserQueries = gql`
  extend type Query {
    users(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): UserSetResponse
    user(id: ID!): User
  }

  extend type Mutation {
    createUser(user: UserInput!): UserMutationResponse!
    updateUser(id: ID!, user: UserInput!): UserMutationResponse!
    deleteUser(id: ID!): UserMutationResponse!
  }
`
