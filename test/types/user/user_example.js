import { BaseType } from "../../../src/baseType"

export class User extends BaseType {
  typeSpec() {
    return {
      name: "User",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: "ID",
          key: true,
          required: true,
          input: false,
        },
        passesTest: {
          type: "Boolean",
        },
      },
    }
  }
}
