import { BaseType, Permissions } from "../../../src/baseType"

export class User extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "User",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: "ID",
          key: true,
          required: true,
          ownerId: true,
          input: false,
        },
        email: {
          type: "String",
          index: true,
          unique: true,
          readPermission: Permissions.Personal,
        },
        userName: {
          type: "String",
          index: true,
          unique: true,
        },
        emailToken: {
          type: "Object",
          objectType: "AuthToken",
          readPermission: Permissions.Personal,
        },
        address: {
          type: "Object",
          objectType: "Address",
        },
        credentials: {
          type: "Object",
          objectType: "Credentials",
          readPermission: Permissions.Internal,
        },
      },
    }
  }
}

export class AuthToken extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "AuthToken",
      extends: null,
      instantiable: false,
      fields: {
        value: {
          type: "String",
        },
        created: {
          type: "DateTime",
        },
      },
    }
  }
}

export class Credentials extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Credentials",
      extends: null,
      instantiable: false,
      fields: {
        bptAuthV1: {
          type: "Object",
          objectType: "BptAuthV1",
        },
        bptLegacy: {
          type: "Object",
          objectType: "BptLegacy",
        },
        ver: {
          type: "Integer",
        },
      },
    }
  }
}
