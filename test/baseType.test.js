import { MockLog } from "./mockLog"
import { Account, AccountClaims, AuthToken } from "./types/account/account"
import { Event } from "./types/event/event"
import { Zipcode } from "./types/zipcode/zipcode"

const nativeAccount = {
  status: "ACTIVE",
  createdDate: "2019-11-20T01:28:25.686Z",
  claims: {
    sub: "glyphstone",
    email: "breanna@brownpapertickets.com",
    email_verified: true,
    name: "Breanna",
    preferred_username: "Her Majesty",
  },
  emailToken: {
    value: "RANDOM-KEY",
    created: "2019-11-20T01:28:25.686Z",
  },
}

const shortAccount1 = {
  id: "5dd496b90423001caeb865a9",
  status: "ACTIVE",
  claims: {
    emailVerified: true,
    preferredUserName: "Call Me B.",
  },
}

const docAccount = {
  _doc: {
    _id: "5dd496b90423001caeb865a9",
    status: "ACTIVE",
    createdDate: "2019-11-20T01:28:25.686Z",
    claims: {
      _doc: {
        _id: "5dd496b90423001caeb865aa",
        sub: "glyphstone",
        email: "breanna@brownpapertickets.com",
        email_verified: true,
        name: "Breanna",
        preferred_username: "Her Majesty",
      },
    },
    emailToken: {
      _doc: {
        _id: "5dd496b90423001caeb865ab",
        value: "RANDOM-KEY",
        created: "2019-11-20T01:28:25.686Z",
      },
    },
  },
}

let container
beforeAll(() => {
  container = { log: new MockLog() }
})

describe("BaseType Tests", () => {
  test("Test LoadNative ", async () => {
    let account = new Account()
    account.loadNative(nativeAccount)
    expect(account).toEqual({
      status: "ACTIVE",
      claims: {
        accountId: "glyphstone",
        email: "breanna@brownpapertickets.com",
        emailVerified: true,
        name: "Breanna",
        preferredUserName: "Her Majesty",
      },
      emailToken: {
        value: "RANDOM-KEY",
        created: "2019-11-20T01:28:25.686Z",
      },
      createdDate: "2019-11-20T01:28:25.686Z",
    })
  })

  test("Test LoadNative Empty", async () => {
    let account = new Account()
    account.loadNative({})
    expect(account).toEqual({})
  })

  test("Test LoadDocument ", async () => {
    let account = new Account()
    account.loadDocument(docAccount)
    console.log(JSON.stringify(account, null, 2))

    let expected = new Account({
      id: "5dd496b90423001caeb865a9",
      status: "ACTIVE",
      claims: new AccountClaims({
        accountId: "glyphstone",
        email: "breanna@brownpapertickets.com",
        emailVerified: true,
        name: "Breanna",
        preferredUserName: "Her Majesty",
      }),
      emailToken: new AuthToken({
        value: "RANDOM-KEY",
        created: "2019-11-20T01:28:25.686Z",
      }),
      createdDate: "2019-11-20T01:28:25.686Z",
    })
    console.log(JSON.stringify(expected, null, 2))

    expect(account).toEqual(expected)
  })

  test("Test ToNativeJson unassigned fields", async () => {
    let account = new Account(shortAccount1)
    const nativeJson = account.toNativeJson()
    console.log(JSON.stringify(nativeJson, null, 2))
    expect(nativeJson).toEqual({
      _id: "5dd496b90423001caeb865a9",
      status: "ACTIVE",
      claims: { email_verified: true, preferred_username: "Call Me B." },
    })
  })

  test("Test zipcode to graphql type", async () => {
    let zipcode = new Zipcode({})
    const typeSchema = zipcode.toGraphQLSchema()
    console.log(typeSchema)
  })
})
