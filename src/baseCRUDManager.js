import { BaseManager } from "./baseManager"
import { Error } from "apollo-server"

export class BaseCRUDManager extends BaseManager {
  constructor(typeName, type, container) {
    super(typeName, type, container)
    this.dataInterfaces = container.dataInterfaces
  }

  // ============== Standard CRUD Methods ======================

  async get(args, { auth }, { doAuth, session }) {
    const data = await this.getAPI().get(args.id, session)
    if (data) {
      await this.authorize("get", doAuth, data, auth)
      const result = await this.applyReadPermissions(doAuth, data, auth)
      return result
    } else {
      return null
    }
  }

  async query(args, { auth }, { doAuth, session }) {
    await this.authorize("query", doAuth, null, auth)
    const results = await this.getAPI().query(
      args.offset,
      args.limit,
      args.filter,
      args.sort,
      session
    )

    results.items = await this.applyQueryReadPermissions(
      doAuth,
      results.items,
      auth
    )

    return results
  }

  async create(data, { auth }, { doAuth, session }) {
    await this.authorize("create", doAuth, null, auth)
    const result = await this.getAPI().create(data, session)
    return {
      success: true,
      message: `${this.typeName} Created`,
      data: result,
    }
  }

  async update(id, data, { auth }, { doAuth, session }) {
    const api = this.getAPI()
    const obj = await api.get(id) // fetch to do authorization
    await this.authorize("update", doAuth, obj, auth)

    const result = await api.update(id, data, session)
    return {
      success: true,
      message: `${this.typeName} Updated`,
      data: result,
    }
  }

  async delete(id, { auth }, { doAuth, session }) {
    const api = this.getAPI()
    const obj = await api.get(id) // fetch to do authorization
    await this.authorize("delete", doAuth, obj, auth)

    let result = await api.delete(id, session)

    const props = Object.getOwnPropertyNames(result)

    if (props.length == 0) {
      return {
        success: false,
        message: `Delete Failed`,
        data: null,
      }
    }

    return {
      success: true,
      message: `${this.typeName} Deleted`,
      data: result,
    }
  }

  // ===================================================
  // Helper methods
  // Many-to-one fetch from another Type
  async getLinked({ obj, byField }, context, options) {
    let result = null

    if (byField in obj && obj[byField] != null) {
      const linkId = obj[byField]
      try {
        result = await this.get({ id: linkId }, context, options)
      } catch (ex) {
        this.log.error(
          `BaseCRUDManager.getLinked Error getting linked object: ${ex.constructor.name} msg: ${ex.message}`
        )
      }
    } else {
      this.log.warn(
        `getLinked byField not found: ${byField} ${JSON.stringify(
          obj,
          null,
          2
        )}`
      )
    }

    return result
  }

  /**
   * Get linked data by 1-M type join
   * param fields:
   * obj: the joining BaseModel object,
   * idField: the name of the field to get the join value from the obj, ( can be one or an array of field names)
   * byForeignField: name or array of field names in the target table to join against.
   * offset: offset to start query (usually zero)
   * limit: a maximum count to return
   * array: true if multiple to be returned in an array or if false will return the zeroeth object.
   * @param {*} param0
   * @param {*} context
   * @param {*} options
   */
  async getLinkedMulti(
    {
      obj,
      idField,
      byForeignField,
      filter,
      sort,
      offset = 0,
      limit = 100,
      array = true,
    },
    context,
    options
  ) {
    idField = idField || "id"
    let result = null
    let keyFilter = this.getKeyFilter(obj, idField, byForeignField)
    if (keyFilter) {
      if (filter) {
        keyFilter = `(${keyFilter}) AND (${filter})`
      }

      try {
        result = await this.query(
          {
            filter: keyFilter,
            sort,
            offset,
            limit,
          },
          context,
          options
        )
      } catch (ex) {
        this.log.error(
          `BaseCRUDManager.getLinked Error getting linked object: ${ex.constructor.name} msg: ${ex.message}`
        )
      }
    } else {
      this.log.warn(`No join key match`)
    }

    if (array) {
      return result ? result.items : null
    } else {
      return result ? result.items[0] : null
    }
  }

  getKeyFilter(obj, idField, foreignIdField) {
    if (!Array.isArray(idField)) {
      idField = [idField]
    }
    if (!Array.isArray(foreignIdField)) {
      foreignIdField = [foreignIdField]
    }
    if (idField.length != foreignIdField.length) {
      throw Error(
        `idField and foreignIdField length must be the same: idField: ${idField} foreignIdField: ${foreignIdField}`
      )
    }

    let filterArray = []
    for (let i = 0; i < idField.length; i += 1) {
      const val = obj[idField[i]]
      filterArray.push(`${foreignIdField[i]} = '${val}'`)
    }

    const keyFilter = filterArray.join(" AND ")

    return keyFilter
  }

  /**
   *
   * @param {*} param0 - {
   * obj: the joining BaseModel object, i
   * dField: the name of the field to get the join value from the obj,
   * relationshipName: to look up additional join specs in target join API config
   * limit: a maximum count to return
   * array: true if multiple to be returned in an array or if false will return the zeroeth object.
   * @param {*} context
   * @param {*} options
   */
  async getLinkedIntermediate(
    { obj, idField, relationshipName, limit, array },
    context,
    options
  ) {
    idField = idField || "id"
    let result = null

    if (idField in obj && obj[idField] != null) {
      const joinKeyValue = obj[idField]

      try {
        result = await this.getAPI().joinByIntermediate(
          relationshipName,
          joinKeyValue,
          limit
        )
        result.items = await this.applyQueryReadPermissions(
          options.doAuth,
          result.items,
          context.auth
        )
      } catch (ex) {
        this.log.error(
          `BaseCRUDManager.joinByIntermediate Error getting linked object: ${ex.constructor.name} msg: ${ex.message}`
        )
      }
    }

    if (array) {
      return result ? result.items : null
    } else {
      return result ? result.items[0] : null
    }
  }
}
