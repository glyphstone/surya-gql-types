import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"
import { isObject } from "util"

export const Permissions = {
  Public: "public", // can be accessed by any user
  Secure: "secure", // can be accessed by a user with Secure privilege on object type
  Personal: "personal", // can be accessed by a user who owns the immediate object
  Internal: "internal", // is not exposed on external api, but is on the API returned object
}

export class BaseType {
  constructor(data) {
    if (data) {
      this.loadData(data, false)
    }
  }

  /**
   * Override typeSpec() to return definition of type global properties and fields.
   */
  typeSpec() {
    return {
      name: "BaseType",
      extends: null,
      instantiable: false,
      timestamps: false,
      permissions: {
        Secure: ["auth"],
      },
      fields: {},
    }
  }

  /** Override childTypes() with list of types for native json conversion */
  childTypes() {
    return {}
  }

  getName() {
    return this.typeSpec().name
  }

  getOwnerField() {
    const spec = this.typeSpec()
    let ownerFieldName = null
    if (spec && spec.fields && isObject(spec.fields)) {
      for (let fieldName of Object.keys(spec.fields)) {
        const fieldSpec = spec.fields[fieldName]
        if (fieldSpec.ownerId) {
          ownerFieldName = fieldName
          break
        }
      }
    }
    return ownerFieldName
  }

  getKeyField() {
    const spec = this.typeSpec()
    let keyFieldName = null
    if (spec && spec.fields && isObject(spec.fields)) {
      for (let fieldName of Object.keys(spec.fields)) {
        const fieldSpec = spec.fields[fieldName]
        if (fieldSpec.key) {
          keyFieldName = fieldName
          break
        }
      }
    }
    return keyFieldName
  }

  getOwnerId() {
    const ownerFieldName = this.getOwnerField()
    return this[ownerFieldName]
  }

  /**
   * Default implementation of isOwner logic.
   * Override this logic in cases where the userId of the owner is not directly stored as a data field of the object
   *
   * @param {*} userId
   */
  isOwner(userId, accountId) {
    return this.getOwnerId() == userId
  }

  getPermissions(permType) {
    const typeSpec = this.typeSpec()
    if (typeSpec.permissions) {
      return typeSpec.permissions[permType]
    }
    return []
  }

  /**
   * Apply permission filters
   * Expect to be called only if user is not Secure (has permissions to Secure fields.)
   * filter out secure fields
   * filter out Personal fields unless the user is the owner
   * @param {*} authUserId
   */
  applyReadPermissions(authUserId, authAccountId) {
    const isOwner = this.isOwner(authUserId, authAccountId)
    let filtered = Object.create(this)
    filtered.loadData(this, false)

    const spec = this.typeSpec()
    if (spec && spec.fields && isObject(spec.fields)) {
      Object.keys(spec.fields).forEach((fieldName) => {
        const fieldSpec = spec.fields[fieldName]
        this._filterDataField(isOwner, fieldName, fieldSpec, filtered)
      })
    }
    return filtered
  }

  _filterDataField(isOwner, fieldName, fieldSpec, filtered) {
    const readPermission = fieldSpec.readPermission || Permissions.Public
    const filter =
      readPermission == Permissions.Secure ||
      (readPermission == Permissions.Personal && !isOwner)
    if (filter) filtered[fieldName] = null
  }

  /**
   * Load basic JSON data per spec
   * @param {*} data
   */
  loadData(data, subAsNative = true) {
    if (data) {
      const spec = this.typeSpec()
      if (spec && spec.fields && isObject(spec.fields)) {
        Object.keys(spec.fields).forEach((fieldName) => {
          const fieldSpec = spec.fields[fieldName]
          this._loadDataField(fieldName, fieldSpec, data, subAsNative)
        })
      }
    }
    return this
  }

  /**
   * TODO: figure out how to merge these three load data functions... later
   * ** This version is different in the sense that it assumes that at the root level
   * the field names have already been renamed by the sql query
   * BUT if there is a sub-object it will have field names that may be aliased by asName
   * for cases of PGSQL objects like Point or JSONB data fields and they should be loaded as per
   * loadNative() Got it?
   *
   * @param {*} fieldName
   * @param {*} fieldSpec
   * @param {*} data
   */
  _loadDataField(fieldName, fieldSpec, data, subAsNative) {
    const useFieldName =
      fieldSpec.type == ScalarTypes.ObjectRef ? fieldSpec.refField : fieldName
    let fieldVal = data[useFieldName]

    // if field is Object type, unpack it as a Native object
    if (subAsNative) {
      if (fieldSpec.type == ScalarTypes.Object && fieldVal) {
        const subTypeName = fieldSpec.objectType
        const subType = this.childTypes()[subTypeName]
        if (subType) {
          if (fieldSpec.array) {
            fieldVal = fieldVal.map((item) => {
              const subObj = new subType(item)
              return subObj.loadNative(item)
            })
          } else {
            const subObj = new subType(fieldVal)
            fieldVal = subObj.loadNative(fieldVal)
          }
        } else {
          throw new Error(`type not found in childTypes: ${subTypeName}`)
        }
      }
    }

    this[useFieldName] = fieldVal
  }

  /**
   * Return an object (JSON) that has the data files permuted as per the asName parameters.
   *
   */
  toNativeJson() {
    let nativeObj = {}
    const spec = this.typeSpec()
    if (spec && spec.fields && isObject(spec.fields)) {
      Object.keys(spec.fields).forEach((fieldName) => {
        const fieldSpec = spec.fields[fieldName]
        this._loadToNativeField(nativeObj, this, fieldName, fieldSpec)
      })
    }
    return nativeObj
  }

  _loadToNativeField(obj, data, fieldName, fieldSpec) {
    const toFieldName = fieldSpec.asName ? fieldSpec.asName : fieldName
    let fieldVal = data[fieldName]
    if (fieldVal === undefined) return

    if (fieldSpec.type == ScalarTypes.Object && fieldVal) {
      const subTypeName = fieldSpec.objectType
      const subType = this.childTypes()[subTypeName]
      if (subType) {
        if (fieldSpec.array) {
          fieldVal = fieldVal.map((item) => {
            const subObj = new subType(item)
            return subObj.toNativeJson()
          })
        } else {
          const subObj = new subType(fieldVal)
          fieldVal = subObj.toNativeJson()
        }
      } else {
        throw new Error(`type not found in childTypes: ${subTypeName}`)
      }
    }
    obj[toFieldName] = fieldVal
  }

  /**
   * Load data from mongo document with necessary modifications
   * TODO: Move this to and adapter model hosted in the API
   * @param {*} data
   */
  loadDocument(data) {
    if (data) {
      const spec = this.typeSpec()
      if (spec && spec.fields && isObject(spec.fields)) {
        Object.keys(spec.fields).forEach((fieldName) => {
          const fieldSpec = spec.fields[fieldName]
          this._loadDocField(data, this, fieldName, fieldSpec)
        })
      }
    }

    return this
  }

  /**
   * TOODO:  We should be able to merge loading docs with loading native json with the exception
   * of needing to get the inner data object (._doc).
   * For safety I'm leaving them as separate functions for now though there is 90% overlap.
   *
   * @param {*} doc
   * @param {*} obj
   * @param {*} fieldName
   * @param {*} fieldSpec
   */
  _loadDocField(doc, obj, fieldName, fieldSpec) {
    if (!doc) return
    const data = doc._doc // Otherwise non-string types are not getting.

    if (!!fieldSpec.key) {
      obj[fieldName] = data._id.toString()
    } else {
      let toFieldName = fieldName
      const fromFieldName = fieldSpec.asName || fieldName
      let fieldVal = data[fromFieldName]
      switch (fieldSpec.type) {
        case ScalarTypes.ObjectRef:
          toFieldName = fieldSpec.refField
          break
        case ScalarTypes.Object:
          const subTypeName = fieldSpec.objectType
          const subType = this.childTypes()[subTypeName]
          if (subType) {
            if (fieldSpec.array) {
              fieldVal = fieldVal.map((item) => {
                const subObj = new subType()
                return subObj.loadDocument(item)
              })
            } else {
              const subObj = new subType()
              fieldVal = subObj.loadDocument(fieldVal)
            }
          } else {
            throw new Error(`type not found in childTypes: ${subTypeName}`)
          }
      }

      obj[toFieldName] = fieldVal
    }
  }

  /**
   * Load Data as per asName.
   * This would be useful for loading up data for return that has perumted names
   * @param {*} data
   */
  loadNative(data) {
    if (data) {
      const spec = this.typeSpec()
      if (spec && spec.fields && isObject(spec.fields)) {
        Object.keys(spec.fields).forEach((fieldName) => {
          const fieldSpec = spec.fields[fieldName]
          this._loadFromNativeField(data, this, fieldName, fieldSpec, data)
        })
      }
    }
    return this
  }

  _loadFromNativeField(data, obj, fieldName, fieldSpec) {
    if (!data) return

    const fromFieldName = fieldSpec.asName || fieldName

    let fieldVal = data[fromFieldName]
    if (fieldSpec.type == ScalarTypes.Object && fieldVal) {
      const subTypeName = fieldSpec.objectType
      const subType = this.childTypes()[subTypeName]
      if (subType) {
        if (fieldSpec.array) {
          fieldVal = fieldVal.map((item) => {
            const subObj = new subType()
            return subObj.loadNative(item)
          })
        } else {
          const subObj = new subType()
          fieldVal = subObj.loadNative(fieldVal)
        }
      } else {
        throw new Error(`type not found in childTypes: ${subTypeName}`)
      }
    }
    obj[fieldName] = fieldVal
  }

  toGraphQLSchema() {
    const spec = this.typeSpec()
    this.schemaTypeName = spec.name
    const typeSchema = this._genType(spec)
    const inputSchema = this._genInput(spec)
    const setResponseSchema = this._genSetResponseType(spec)
    const mutationResponseSchema = this._genMutationResponseType(spec)
    const specInputSchemas = this._genSpecInputs(spec)
    const inlineScalars = this._genScalars(spec)
    // console.log(`Scalars [${this.schemaTypeName}]: \n> ${inlineScalars};`)
    const schemas =
      typeSchema +
      "\n" +
      inputSchema +
      "\n" +
      setResponseSchema +
      "\n" +
      mutationResponseSchema +
      "\n" +
      specInputSchemas +
      "\n" +
      inlineScalars +
      "\n"

    return schemas
  }

  _genType(spec) {
    return `type ${spec.name} {
${this._genFields(spec.fields, false, null)}
}`
  }

  _genInput(spec) {
    return `input ${spec.name}Input {
${this._genFields(spec.fields, true, null)}
}`
  }

  _genSpecInputs(spec) {
    const inputNames = spec.inputs || []
    let specInputs = []
    for (let n = 0; n < inputNames.length; n++) {
      const inputName = inputNames[n]
      const specInput = `input ${inputName} {
        ${this._genFields(spec.fields, true, inputName)}
        }`
      specInputs.push(specInput)
    }
    return specInputs.join("/n")
  }

  _genSetResponseType(spec) {
    if (!!spec.instantiable) {
      return `type ${spec.name}SetResponse {
  items: [${spec.name}]!
  total: Int
  count: Int
  offset: Int
}`
    } else {
      return ""
    }
  }

  _genMutationResponseType(spec) {
    if (!!spec.instantiable) {
      return `type ${spec.name}MutationResponse {
  success: Boolean!
  message: String
  data: ${spec.name}
}
`
    } else {
      return ""
    }
  }

  _genFields(fields, input = false, specInput = null) {
    let fstr = Object.getOwnPropertyNames(fields)
      .map((name) => {
        const typedef = fields[name]
        const { fieldName, fieldType, include, params, scalar } = this._typeMap(
          name,
          typedef,
          input,
          specInput
        )
        if (include) {
          return `    ${fieldName}${params}: ${fieldType}\n`
        } else {
          return ""
        }
      })
      .join(" ")
    return fstr
  }

  _genScalars(spec) {
    const fields = spec.fields
    let fstr = Object.getOwnPropertyNames(fields)
      .map((name) => {
        const typedef = fields[name]
        const { fieldName, fieldType, include, params, scalar } = this._typeMap(
          name,
          typedef,
          false,
          ""
        )
        if (scalar.length > 0) {
          return `${scalar}\n`
        }
      })
      .join(" ")
    return fstr
  }

  _typeMap(fieldName, typeDef, input = false, specInput = null) {
    const typeName = typeDef.type || ScalarTypes.String
    const required = !!typeDef.required
    const permission = typeDef.readPermission || Permissions.Public

    let include =
      (!input || (input && typeDef.input !== false)) &&
      permission != Permissions.Internal

    let pre = ""
    let post = ""
    let params = ""
    if (!input) {
      params = typeDef.params || ""
    }

    if (typeDef.array) {
      pre = "["
      post = "]"
    }
    const bang = required ? "!" : ""
    let useFieldName = fieldName
    let scalar = "" // auxiliar scalars such as enums

    let gqlType = "String"
    switch (typeName) {
      case ScalarTypes.String:
        gqlType = pre + "String" + bang + post
        break
      case ScalarTypes.DateTime:
        gqlType = pre + "Date" + bang + post
        break
      case ScalarTypes.Integer:
        gqlType = pre + "Int" + bang + post
        break
      case ScalarTypes.Float:
        gqlType = pre + "Float" + bang + post
        break
      case ScalarTypes.ID:
        gqlType = pre + "ID" + bang + post
        break
      case ScalarTypes.Boolean:
        gqlType = pre + "Boolean" + bang + post
        break
      case ScalarTypes.Object:
        let objectTypeName = typeDef.objectType + (input ? "Input" : "")
        gqlType = pre + objectTypeName + bang + post
        break
      case ScalarTypes.ObjectRef:
        const paramType = typeDef.params || ""

        if (input) {
          const specInputs = typeDef.inputs || []
          if (specInput && specInputs.includes(specInput)) {
            // embed as if it were an input object
            let objTypeName = typeDef.objectType + (input ? "Input" : "")
            gqlType = pre + objTypeName + bang + post
          } else {
            // A usual input id reference
            if (typeDef.refField) {
              useFieldName = typeDef.refField
              gqlType = pre + typeDef.refFieldType + bang + post
            } else {
              // if no refField specified, we assume it's not insertable
              include = false
            }
          }
        } else {
          gqlType = pre + typeDef.objectType + bang + post
          if (paramType == "query") {
            params = "(offset: Int, limit: Int, filter: String, sort: String)"
          }
        }
        break
      case ScalarTypes.Enum:
        let enumName
        if (typeDef.name) {
          enumName = typeDef.name
        } else {
          if (typeDef.values) {
            enumName = `${this.schemaTypeName}_${useFieldName}_ENUM`.toUpperCase()
            const scalarEnum = typeDef.values.join("\n")
            scalar = `enum ${enumName} {
    ${scalarEnum}
    }`
          } else {
            throw new Error(
              `Enum must include "values" list or "name" specified`
            )
          }
        }

        gqlType = pre + enumName + bang + post
        break
      default:
        // Assumed to be a custom Scalar Type.
        gqlType = pre + typeName + bang + post
        break
    }
    return {
      fieldName: useFieldName,
      fieldType: gqlType,
      include: include,
      params,
      scalar,
    }
  }
}
