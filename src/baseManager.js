import { AuthenticationError, ForbiddenError } from "apollo-server"

const authorized = {
  get: { groups: ["admin"] },
  query: { groups: ["admin"] },
  create: { groups: ["admin"] },
  update: { groups: ["admin"] },
  delete: { groups: ["admin"] },
}

export class BaseManager {
  constructor(typeName, type, container) {
    this.typeName = typeName
    this.type = type
    this.container = container
    this.log = container.log
    this.sessionManager = container.sessionManager
    this.api = null
  }

  authorized() {
    return authorized
  }

  getNewInstance() {
    return new this.type()
  }

  setAPI(api) {
    this.api = api
  }

  getAPI() {
    return this.api
  }

  // ===================================================
  // perform authentication and authorization

  async authorize(action, doAuthorize, obj, auth, returnResult = false) {
    if (!doAuthorize) return false

    const sessionData = await this.sessionManager.get(auth)
    if (!sessionData) {
      if (returnResult) {
        return false
      } else {
        throw new AuthenticationError("User session is not authenticated")
      }
    }

    const actionAuth = this.authorized()[action] || {}

    const checkOwner = actionAuth.owner || false
    let isOwner = true
    if (checkOwner) {
      if (obj) {
        const authUserId = await sessionData.getUserId()
        const authAccountId = await sessionData.getAccountId()
        isOwner = obj.isOwner(authUserId, authAccountId)
      } else {
        this.log.warn(
          `Authorize: Cannot check ownership of non-existent object: ${this.typeName}`
        )
        isOwner = false
      }
    }

    const isInGroups = await this.userInGroups(actionAuth.groups, sessionData)

    if (!(isInGroups || (checkOwner && isOwner))) {
      if (returnResult) {
        return false
      } else {
        throw new ForbiddenError("User is not authorized ")
      }
    }
    return true
  }

  async applyReadPermissions(doAuthorize, obj, auth) {
    if (!doAuthorize) {
      return obj
    }

    let sessionData = await this.sessionManager.get(auth)
    if (!sessionData) {
      throw new AuthenticationError("User session is not authenticated")
    }

    const authUserId = await sessionData.getUserId()
    const authAccountId = await sessionData.getAccountId()
    const secureGroups = obj.getPermissions("secure")
    const typeName = obj.constructor.name

    let isInSecureGroups = await this.userInGroups(secureGroups, sessionData)
    if (isInSecureGroups) {
      return obj
    } else {
      return obj.applyReadPermissions(authUserId, authAccountId)
    }
  }

  async applyQueryReadPermissions(doAuthorize, items, auth) {
    if (!doAuthorize) return items

    let sessionData = await this.sessionManager.get(auth)
    if (!sessionData) {
      throw new AuthenticationError("User session is not authenticated")
    }

    const authUserId = await sessionData.getUserId()
    const authAccountId = await sessionData.getAccountId()
    const obj = this.getNewInstance()
    const secureGroups = obj.getPermissions("secure")

    const isInSecureGroups = await this.userInGroups(secureGroups, sessionData)

    if (!isInSecureGroups) {
      let filtered = items.map((obj) => {
        return obj.applyReadPermissions(authUserId, authAccountId)
      })
      return filtered
    } else {
      return items
    }
  }

  /**
   * Allow for future softening of user->group membership system
   */
  async userInGroups(groupList, sessionData) {
    for (let group of groupList) {
      if (group === "*") {
        return true
      } else {
        if (await sessionData.isInGroup(group)) {
          return true
        }
      }
    }

    return false
  }
}
